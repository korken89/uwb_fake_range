#include "ros/ros.h"
#include "geometry_msgs/TransformStamped.h"
#include "uwb_comm/uwb_distance_measurement.h"
#include <string>
#include <vector>
#include <cmath>

struct node_info {
    uint64_t id;
    float x;
    float y;
    float z;
};

std::vector<node_info> nodes;
ros::Time last_mc;
double measurement_rate;
int curr_node;
ros::Publisher _pubDist;

double nodeandtransformtodistance(
        const geometry_msgs::TransformStampedConstPtr &msg,
        const node_info &ni)
{
    double dx = msg->transform.translation.x - ni.x;
    double dy = msg->transform.translation.y - ni.y;
    double dz = msg->transform.translation.z - ni.z;

    return std::sqrt(dx*dx + dy*dy + dz*dz);
}

void subMotionCapture(
        const geometry_msgs::TransformStampedConstPtr &msg)
{
    ros::Time now = ros::Time::now();
    if ((now - last_mc).toSec() >= (1.0 / measurement_rate))
    {
        last_mc = now;

        uwb_comm::uwb_distance_measurement uwb_msg;

        if (nodes.size() > 0)
        {
            uwb_msg.header.stamp = ros::Time::now();
            uwb_msg.header.frame_id = "/world";
            uwb_msg.src = curr_node;
            uwb_msg.dist = nodeandtransformtodistance(msg, nodes[curr_node]);
            uwb_msg.anchor_pos.x = nodes[curr_node].x;
            uwb_msg.anchor_pos.y = nodes[curr_node].y;
            uwb_msg.anchor_pos.z = nodes[curr_node].z;
            uwb_msg.covariance = 0.04*0.04;
            uwb_msg.rssi = -50;

            _pubDist.publish( uwb_msg );

            curr_node++;
            if (curr_node >= nodes.size())
                curr_node = 0;
        }
    }
}

std::vector<std::string> split(std::string data, std::string token)
{
    std::vector<std::string> output;
    size_t pos = std::string::npos;
    do
    {
        pos = data.find(token);
        output.push_back(data.substr(0, pos));
        if (std::string::npos != pos)
            data = data.substr(pos + token.size());
    } while (std::string::npos != pos);
    return output;
}

void initAnchorPositions(ros::NodeHandle &nh)
{
    /* Detected. */
    int node_number = 0;
    //enumerate all parameters
    std::string temp = "";

    while(nh.getParam("node_" + std::to_string(node_number), temp))
    {
        ROS_INFO("Found node information: \"%s\" ",temp.c_str());
        // parse data and put into string
        std::vector<std::string> nodeinfo(4);
        nodeinfo = split(temp, ",");
        node_info temp_node;
        temp_node.id = stoi(nodeinfo[0]);
        temp_node.x  = stof(nodeinfo[1]);
        temp_node.y  = stof(nodeinfo[2]);
        temp_node.z  = stof(nodeinfo[3]);
        nodes.push_back(temp_node);
        node_number++;
    }
}

/**
 * @brief   Main function.
 *
 * @param[in] argc  Number of arguments.
 * @param[in] argv  List with the arguments.
 *
 * @return Status code.
 */
int main(int argc, char *argv[])
{
    /* Initialize ROS. */
    ros::init(argc, argv, "uwb_fake_range_node");
    ros::NodeHandle n;
    ros::NodeHandle nh("~");

    last_mc = ros::Time::now();
    curr_node = 0;

    /* Get parameters */
    std::string prefix;
    std::string motioncapture;

    initAnchorPositions(nh);

    nh.getParam("prefix", prefix);

    if (prefix.size() == 0)
    {
        prefix = ros::this_node::getName();
        ROS_WARN("No prefix specified, using node name.");
    }

    if (prefix.front() == '/')
        prefix.erase(0, 1);

    if (prefix.back() != '/')
        prefix += "/";

    ROS_INFO("Fake UWB prefix:   %s", prefix.c_str());

    nh.getParam("motioncapture_publisher", motioncapture);

    if (motioncapture.size() == 0)
    {
        ROS_ERROR("No motion capture reference selected, aborting!");
        return -1;
    }

    nh.getParam("measurement_rate", measurement_rate);

    if (measurement_rate < 1.0)
    {
        ROS_ERROR("No measurement rate set, defaulting to 20 Hz.");
        measurement_rate = 20.0;
    }
    else
        ROS_INFO("Fake UWB rate:     %f Hz", measurement_rate);

    _pubDist = n.advertise<uwb_comm::uwb_distance_measurement>(
                                        prefix + "range",
                                        10);

    ros::Subscriber _subMotionCapture = n.subscribe(motioncapture,
                                        10,
                                        subMotionCapture,
                                        ros::TransportHints().tcpNoDelay());

    ros::spin();

    return 0;
}
